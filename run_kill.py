from subprocess import call
import os
dir = os.getcwd()

call("ss -lptn 'sport = :3366' > kill_process.txt", shell=True)
f = open(dir+'/kill_process.txt')
text = f.read()
if 'pid' in text:
    ls_temp = text.split(',')
    for i in ls_temp:
        if 'pid' in i:
            print('Kill pid', i.replace('pid=', ''))
            call("kill " + i.replace('pid=', ''), shell=True)
call("rm kill_process.txt", shell=True)
